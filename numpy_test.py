import numpy as np


 # 2. Project all points to the plane2

pt = np.array([1, 2, 3])
normal = np.array([1, 1, 1])
xyz = np.arange(15).reshape(5, 3)

print("xyz = ", xyz)

vec2pt = xyz - pt
distances = vec2pt.dot(normal)

print("distances = ", distances)
print("normal = ", normal)

test1 = np.ones(xyz.shape).dot(distances.reshape(1, ))

print("test1 = ", test1)
print("xyz = ", xyz)

print("np.ones(xyz.shape).dot(normal) = ", np.ones(xyz.shape).dot(normal))

print("np.ones(xyz.shape)*normal = ", np.ones(xyz.shape)*normal)

