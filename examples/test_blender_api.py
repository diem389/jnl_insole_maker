import bpy
from bpy import context

# Tip 1: Cursor Locations 
# Get the current scene
scene = context.scene

# Get the 3D cursor
cursor = scene.cursor_location

# Get the active object (assume we have one)
obj = scene.objects.active

# Now make a copy of the object
obj_new = obj.copy()

# The object won't automatically get into a new scene
scene.objects.link(obj_new)

# Now we can place the object
obj_new.location = cursor