#----------------------------------------------------------
# File lattice.py
#----------------------------------------------------------
import bpy
import os
def createIcoSphere(origin):
    # Create an icosphere
    bpy.ops.mesh.primitive_ico_sphere_add(location=origin)
    ob = bpy.context.object
    me = ob.data
    # Create vertex groups
    upper = ob.add_vertex_group('Upper')
    lower = ob.add_vertex_group('Lower')
    for v in me.verts:
        if v.co[2] > 0.001:
            # ob.add_vertex_to_group(v.index, upper, 1.0, 'REPLACE')
            upper.add(v.index, 1.0, 'REPLACE')
        elif v.co[2] < -0.001:            
            # ob.add_vertex_to_group(v.index, lower, 1.0, 'REPLACE')
            lower.add(v.index, 1.0, 'REPLACE')
        else:
            # ob.add_vertex_to_group(v.index, upper, 0.5, 'REPLACE')
            # ob.add_vertex_to_group(v.index, lower, 0.5, 'REPLACE')
            upper.add(v.index, 0.5, 'REPLACE')
            lower.add(v.index, 0.5, 'REPLACE')
    return ob
    
def createLattice(origin):
    # Create lattice and object
    lat = bpy.data.lattices.new('MyLattice')
    ob = bpy.data.objects.new('LatticeObject', lat)
    ob.location = origin
    ob.x_ray = True
    # Link object to scene
    scn = bpy.context.scene
    scn.objects.link(ob)
    scn.objects.active = ob
    scn.update()
    # Set lattice attributes
    lat.interpolation_type_u = 'KEY_LINEAR'
    lat.interpolation_type_v = 'KEY_CARDINAL'
    lat.interpolation_type_w = 'KEY_BSPLINE'
    lat.outside = False
    lat.points_u = 2
    lat.points_v = 2
    lat.points_w = 2

    # Set lattice points
    s = 1.0
    points = [
        (-s,-s,-s), (s,-s,-s), (-s,s,-s), (s,s,-s),
        (-s,-s,s), (s,-s,s), (-s,s,s), (s,s,s)
    ]
    for n,pt in enumerate(lat.points):
        for k in range(3):
            pt.co[k] = points[n][k]
    return ob

def run(origin):
    sphere = createIcoSphere(origin)
    lat = createLattice(origin)
    # Create lattice modifier
    mod = sphere.modifiers.new('Lat', 'LATTICE')
    mod.object = lat
    mod.vertex_group = 'Upper'
    # Lattice in edit mode for easy deform
    bpy.context.scene.update()
    bpy.ops.object.mode_set(mode='EDIT')
    return

if __name__ == "__main__":
    run((0,0,0))
