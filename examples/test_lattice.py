__author__ = 'Jeff'

import bpy

'''
Created by Jimmy Gunawan
Last update: 20130422
Original Source: Blender Sushi

FUNCTION:
    Automatically create and confirm Lattice position to selected object as
    Lattice Modifier.

HOW TO USE:
    1. Select a mesh that you like to be Latticed
    2. Run the script
    3. Select Lattice, go to Edit Mode, deform the object
    4. Under Lattice Object Data Property, you also have extra options
    for Lattice object.

'''

object = bpy.context.active_object
object_name = object.name


# Get Rotation of active object
rotXYZ = object.rotation_euler

# Get Dimension of active object
dimensionXYZ = object.dimensions

# REFERENCE:
# http://blenderscripting.blogspot.com.au/2011/05/blender-25-python-moving-object-origin.html
# https://github.com/davidejones/alternativa3d_tools/issues/1

# Snap Cursor to Active
original_type = bpy.context.area.type
bpy.context.area.type = "VIEW_3D"
bpy.ops.view3d.snap_cursor_to_active()
bpy.context.area.type = original_type

# Save the current location of our object origin
saved_location = bpy.context.scene.cursor_location.copy()  # returns a copy of the vector

# Snap origin to geometry center median
bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')

# Snap Cursor to Active Again
original_type = bpy.context.area.type
bpy.context.area.type = "VIEW_3D"
bpy.ops.view3d.snap_cursor_to_active()
bpy.context.area.type = original_type

# Get Current Origin
center_location = bpy.context.scene.cursor_location.copy()

# Put Cursor Back At Original Location of Object Origin
bpy.context.scene.cursor_location = saved_location

# Set the origin on the current object to the 3d cursor location
bpy.ops.object.origin_set(type='ORIGIN_CURSOR')

# Add and place our Lattice
bpy.ops.object.add(type='LATTICE', view_align=False, enter_editmode=False, location=center_location, rotation=rotXYZ)

# Get Our Lattice
lattice = bpy.context.active_object
lattice_name = lattice.name

# Scale Lattice to Dimension of Object
lattice.scale = dimensionXYZ

# Return our object as active
bpy.context.scene.objects.active = bpy.data.objects[object_name]

# Apply Lattice Modifier
bpy.ops.object.modifier_add(type='LATTICE')
bpy.data.objects[object_name].modifiers["Lattice"].object = bpy.data.objects[lattice_name]
