

class Employee:
	"""docstring for Employee"""
	count = 0
	def __init__(self, name):
		self.count += 1
		self.name = name

	def display_employee(self):
		print("Hello? %d'th man, %s"%(self.count+1, self.name))


if __name__ == '__main__':
	
	print("Test #01  -------------------------------------------------------")

	a = Employee("Jeff")
	b = Employee("James")
	c = Employee("Kick")

	a.display_employee()
	b.display_employee()
	c.display_employee()

	print("* Test #02  -------------------------------------------------------")

	python_webframeworks = ['flask','django', 'pylons','pyramid','brubeck']
	print('flask' in python_webframeworks)

	print("* Test #03  -------------------------------------------------------")
	python_webframeworks_d = {'flask':1,'django':2, 
		'pylons':3,'pyramid':4,'brubeck':5}

	print('django' in python_webframeworks_d)

	
	print("* Test #04  -------------------------------------------------------")

	d = {}
	d[1] = "abc"
	d[3] = 'def'
	print(len(d))
	print(d)

	import operator
	x = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
	sorted_x = sorted(x.items(), key=operator.itemgetter(1))
	print(sorted_x)