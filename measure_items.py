import bpy

from mathutils import *
import bgl
import blf

from bpy_extras.view3d_utils import location_3d_to_region_2d
from . import utils_insole_maker as utils
from . import utils_drawing as ud

# Global Objects
_measure_items = []


def add_display_item(display_item):
    _measure_items.append(display_item)


def clear_display():
    _measure_items.clear()


def set_display_item(item, index = -1):

    if index < 0:
        _measure_items.clear()
        _measure_items.append(item)
    else:
        _measure_items[index] = item


def get_display_items():
    return _measure_items


# Classes 
class MeasureItem:
    def __init__(self):
        self.name = ""
        pass

    # bgl draw
    def display(self, context):
        pass


class Length(MeasureItem):
    def __init__(self, name="Length", p1=Vector(), p2=Vector()):
        self.p1 = p1
        self.p2 = p2
        self.name = name

    def display(self, context):

        line_width_xyz = 1
        line_width_dist = 2
        color = (0.5, 0.0, 1.0, 0.8)

        # 3D View - text offset
        offset_line = 10  # Offset the text a bit to the right.
        offset_y = 15  # Offset of the lines.
        offset_value = 50  # Offset of value(s) from the text.

        # Get & convert the Perspective Matrix of the current view/region.
        view3d = bpy.context
        region = view3d.region_data
        perspective_matrix = region.perspective_matrix
        temp_mat = [perspective_matrix[j][i] for i in range(4) for j in
                    range(4)]
        perspective_buff = bgl.Buffer(bgl.GL_FLOAT, 16, temp_mat)

        # ---
        # Store previous OpenGL settings.
        # Store MatrixMode
        matrix_mode_prev = bgl.Buffer(bgl.GL_INT, [1])
        bgl.glGetIntegerv(bgl.GL_MATRIX_MODE, matrix_mode_prev)
        matrix_mode_prev = matrix_mode_prev[0]

        # Store projection matrix
        projection_matrix_prev = bgl.Buffer(bgl.GL_DOUBLE, [16])
        bgl.glGetFloatv(bgl.GL_PROJECTION_MATRIX, projection_matrix_prev)

        # Store Line width
        line_width_prev = bgl.Buffer(bgl.GL_FLOAT, [1])
        bgl.glGetFloatv(bgl.GL_LINE_WIDTH, line_width_prev)
        line_width_prev = line_width_prev[0]

        # Store GL_BLEND
        blend_prev = bgl.Buffer(bgl.GL_BYTE, [1])
        bgl.glGetFloatv(bgl.GL_BLEND, blend_prev)
        blend_prev = blend_prev[0]

        line_stipple_prev = bgl.Buffer(bgl.GL_BYTE, [1])
        bgl.glGetFloatv(bgl.GL_LINE_STIPPLE, line_stipple_prev)
        line_stipple_prev = line_stipple_prev[0]

        # Store glColor4f
        color_prev = bgl.Buffer(bgl.GL_FLOAT, [4])
        bgl.glGetFloatv(bgl.GL_COLOR, color_prev)

        # ---
        # Prepare for 3D drawing
        bgl.glLoadIdentity()
        bgl.glMatrixMode(bgl.GL_PROJECTION)
        bgl.glLoadMatrixf(perspective_buff)

        bgl.glEnable(bgl.GL_BLEND)
        bgl.glEnable(bgl.GL_LINE_STIPPLE)

        # ---
        # Draw 3D stuff.
        bgl.glLineWidth(line_width_xyz)

        p1 = self.p1
        p2 = self.p2
        dist = (p1 - p2).length

        # X
        bgl.glColor4f(1, 0, 0, 0.8)
        bgl.glBegin(bgl.GL_LINE_STRIP)
        bgl.glVertex3f(p1[0], p1[1], p1[2])
        bgl.glVertex3f(p2[0], p1[1], p1[2])
        bgl.glEnd()
        # Y
        bgl.glColor4f(0, 1, 0, 0.8)
        bgl.glBegin(bgl.GL_LINE_STRIP)
        bgl.glVertex3f(p1[0], p1[1], p1[2])
        bgl.glVertex3f(p1[0], p2[1], p1[2])
        bgl.glEnd()
        # Z
        bgl.glColor4f(0, 0, 1, 0.8)
        bgl.glBegin(bgl.GL_LINE_STRIP)
        bgl.glVertex3f(p1[0], p1[1], p1[2])
        bgl.glVertex3f(p1[0], p1[1], p2[2])
        bgl.glEnd()

        # Dist
        bgl.glLineWidth(line_width_dist)
        bgl.glColor4f(color[0], color[1], color[2], color[3])
        bgl.glBegin(bgl.GL_LINE_STRIP)
        bgl.glVertex3f(p1[0], p1[1], p1[2])
        bgl.glVertex3f(p2[0], p2[1], p2[2])
        bgl.glEnd()

        # ---
        # Restore previous OpenGL settings
        bgl.glLoadIdentity()
        bgl.glMatrixMode(matrix_mode_prev)
        bgl.glLoadMatrixf(projection_matrix_prev)
        bgl.glLineWidth(line_width_prev)
        if not blend_prev:
            bgl.glDisable(bgl.GL_BLEND)
        if not line_stipple_prev:
            bgl.glDisable(bgl.GL_LINE_STIPPLE)
        bgl.glColor4f(
            color_prev[0],
            color_prev[1],
            color_prev[2],
            color_prev[3])

        # ---
        # Draw (2D) text
        # We do this after drawing the lines so
        # we can draw it OVER the line.
        coord_2d = location_3d_to_region_2d(
            context.region,
            context.space_data.region_3d,
            p1.lerp(p2, 0.5))

        texts = [
            ("Dist.:", dist),
            ("DX:", abs(p1[0] - p2[0])),
            ("DY:", abs(p1[1] - p2[1])),
            ("DZ:", abs(p1[2] - p2[2])),
        ]

        if self.name == "Breadth":
            texts.insert(0, ("----------", "---------------"))
            texts.insert(0, (self.name + ":", abs(p1[0] - p2[0])))
        elif self.name == "Length":
            texts.insert(0, ("----------", "---------------"))
            texts.insert(0, (self.name + ":", abs(p1[1] - p2[1])))
        else:
            print("Warning, unknown drawing object")

        # Draw all texts
        bgl.glColor4f(1.0, 1.0, 1.0, 1.0)
        blf.size(0, 12, 72)  # Prevent font size to randomly change.

        # uinfo = utils.get_units_info()

        loc_x = coord_2d[0] + offset_line
        loc_y = coord_2d[1]

        for t in texts:
            text = t[0]
            if type(t[1]) is str:
                value = t[1]
            else:
                # value = utils.convert_distance(t[1], u_info)
                value = "%.2f mm"%(t[1],)

            blf.position(0, loc_x, loc_y, 0)
            blf.draw(0, text)
            blf.position(0, loc_x + offset_value, loc_y, 0)
            blf.draw(0, value)

            loc_y -= offset_y


class ArchInfo(MeasureItem):
    def __init__(self, measure_result):
        self.name = "Arch"
        self.measure_result = measure_result

    def display(self, context):

        line_width_xyz = 1
        line_width_dist = 2
        color = (0.5, 0.0, 1.0, 0.8)
        color_red = (1.0, 0.0, 0.0, 0.8)
        color_green = (0.0, 1.0, 0.0, 0.8)
        color_blue = (0.0, 0.0, 1.0, 0.8)

        # 3D View - text offset
        offset_line = 10  # Offset the text a bit to the right.
        offset_y = 15  # Offset of the lines.
        offset_value = 100  # Offset of value(s) from the text.

        # Get & convert the Perspective Matrix of the current view/region.
        view3d = bpy.context
        region = view3d.region_data
        perspective_matrix = region.perspective_matrix
        temp_mat = [perspective_matrix[j][i] for i in range(4) for j in
                    range(4)]
        perspective_buff = bgl.Buffer(bgl.GL_FLOAT, 16, temp_mat)

        # ---
        # Store previous OpenGL settings.
        # Store MatrixMode
        matrix_mode_prev = bgl.Buffer(bgl.GL_INT, [1])
        bgl.glGetIntegerv(bgl.GL_MATRIX_MODE, matrix_mode_prev)
        matrix_mode_prev = matrix_mode_prev[0]

        # Store projection matrix
        projection_matrix_prev = bgl.Buffer(bgl.GL_DOUBLE, [16])
        bgl.glGetFloatv(bgl.GL_PROJECTION_MATRIX, projection_matrix_prev)

        # Store Line width
        line_width_prev = bgl.Buffer(bgl.GL_FLOAT, [1])
        bgl.glGetFloatv(bgl.GL_LINE_WIDTH, line_width_prev)
        line_width_prev = line_width_prev[0]

        # Store GL_BLEND
        blend_prev = bgl.Buffer(bgl.GL_BYTE, [1])
        bgl.glGetFloatv(bgl.GL_BLEND, blend_prev)
        blend_prev = blend_prev[0]

        line_stipple_prev = bgl.Buffer(bgl.GL_BYTE, [1])
        bgl.glGetFloatv(bgl.GL_LINE_STIPPLE, line_stipple_prev)
        line_stipple_prev = line_stipple_prev[0]

        # Store glColor4f
        color_prev = bgl.Buffer(bgl.GL_FLOAT, [4])
        bgl.glGetFloatv(bgl.GL_COLOR, color_prev)

        # ---
        # Prepare for 3D drawing
        bgl.glLoadIdentity()
        bgl.glMatrixMode(bgl.GL_PROJECTION)
        bgl.glLoadMatrixf(perspective_buff)

        bgl.glEnable(bgl.GL_BLEND)
        bgl.glEnable(bgl.GL_LINE_STIPPLE)

        # ---
        # Draw 3D stuff.
        bgl.glLineWidth(line_width_xyz)

        def draw_line_cross_center(p1, p2, p_center,
                                   line_color=color, axis_id=0):
            # Dist
            bgl.glLineWidth(line_width_dist)
            bgl.glColor4f(line_color[0], line_color[1],
                          line_color[2], line_color[3])
            bgl.glBegin(bgl.GL_LINE_STRIP)
            if axis_id == 0:
                bgl.glVertex3f(p1[0], p_center[1], p_center[2])
                bgl.glVertex3f(p2[0], p_center[1], p_center[2])
            elif axis_id == 1:
                bgl.glVertex3f(p_center[0], p1[1], p_center[2])
                bgl.glVertex3f(p_center[0], p2[1], p_center[2])
            elif axis_id == 2:
                bgl.glVertex3f(p_center[0], p_center[1], p1[2])
                bgl.glVertex3f(p_center[0], p_center[1], p2[2])
            else:
                print("[Error] unsupported axis_id")

            bgl.glEnd()

        mr = self.measure_result
        draw_line_cross_center(mr.arch_x_min_pos, mr.arch_x_max_pos,
                               mr.arch_center, color_red)
        draw_line_cross_center(mr.arch_y_min_pos, mr.arch_y_max_pos,
                               mr.arch_center, color_green, axis_id=1)
        draw_line_cross_center(mr.arch_z_min_pos, mr.arch_z_max_pos,
                               mr.arch_center, color_blue, axis_id=2)

        # ---
        # Restore previous OpenGL settings
        bgl.glLoadIdentity()
        bgl.glMatrixMode(matrix_mode_prev)
        bgl.glLoadMatrixf(projection_matrix_prev)
        bgl.glLineWidth(line_width_prev)
        if not blend_prev:
            bgl.glDisable(bgl.GL_BLEND)
        if not line_stipple_prev:
            bgl.glDisable(bgl.GL_LINE_STIPPLE)
        bgl.glColor4f(
            color_prev[0],
            color_prev[1],
            color_prev[2],
            color_prev[3])

        # ---
        # Draw (2D) text
        # We do this after drawing the lines so
        # we can draw it OVER the line.
        coord_2d = location_3d_to_region_2d(context.region,
                                            context.space_data.region_3d,
                                            mr.arch_center)


        dx = abs(mr.arch_x_max_pos[0] - mr.arch_x_min_pos[0])
        dy = abs(mr.arch_y_max_pos[1] - mr.arch_y_min_pos[1])

        texts = [
            ("*Arch Info.", ""),
            ("------------", "---------------"),
            (" Height:", abs(mr.arch_center[2]))]

        if dx > 0.01:
            texts.append((" Breadth(dx):", dx))

        if dy > 0.01:
            texts.append((" Length(dy):", dy))
    

        # Draw all texts
        bgl.glColor4f(1.0, 1.0, 1.0, 1.0)
        blf.size(0, 12, 72)  # Prevent font size to randomly change.

        # u_info = utils.get_units_info()

        loc_x = coord_2d[0] + offset_line
        loc_y = coord_2d[1]

        for t in texts:
            text = t[0]
            if type(t[1]) is str:
                value = t[1]
            else:
                # value = utils.convert_distance(t[1], u_info)
                value = "%.2f mm"%(t[1],)
                

            blf.position(0, loc_x, loc_y, 0)
            blf.draw(0, text)
            blf.position(0, loc_x + offset_value, loc_y, 0)
            blf.draw(0, value)

            loc_y -= offset_y

# Standard Reference Database Display (SRDB, 참조표준 )
class SrDisplay(MeasureItem):

    def __init__(self, item_name):

        self.name = item_name
        self.line_height = ud.txt_height('A')
        self.width = 200
        self.text_lines     = [" KRISS SRD FOOT DB "]
        self.text_lines.append("---------------------------------")
        self.text_size = 12
        self.spacer = 7  # pixels between text lines
        self.text_dpi = bpy.context.user_preferences.system.dpi
        self.offset_y = 10 
        self.graph_height = 200
        blf.size(0, self.text_size, self.text_dpi)

        # Fill text 

        if self.name == "Breadth":
            self.text_lines.append("Foot Breadth Info.:")
        elif self.name == "Length":
            self.text_lines.append("Foot Length Info.:")
        elif self.name == "Arch":
            self.text_lines.append("Arch Height Info.:")
        else:
            assert False 

        self.srd = {}
        group = bpy.data.groups.get("SRDB")
        if group is not None:
            self.srd = dict(group.get(self.name)) #SR Data dictionary 
       
        try:
            srd = self.srd
            lines = self.text_lines

            lines.append("   - Sample Size: {}".format(srd["Sample"]))
            lines.append("   - Average Value: {} mm".format(srd["Ave."]))
            lines.append("   - Sigma: {}".format(srd["Std."]))
            lines.append("   - Min: {} mm, "
                            "Max: {} mm".format(srd["Min"], srd["Max"]))

        except Exception as e:
            self.text_lines.append("Error! KRISS DB access failed.")
            print(e)
            print("Error: Current DB is ... ")
            print(srd)
            print("Error ---------------    ")
           

    def display(self, context):

        # find parameters for drawing
        txtcol = bpy.context.user_preferences.themes[0].user_interface.\
            wcol_menu_item.text
        txR = txtcol[0]
        txG = txtcol[1]
        txB = txtcol[2]
        txA = 0.9
        txt_color = (txR, txG, txB, txA)
        line_height = ud.txt_height('A')
        # compute left & top 
        screen_width, screen_height = ud.get_3dview_size()
        left = screen_width - self.width
        top = screen_height - self.offset_y

        txt_y = 0
        for i, line in enumerate(self.text_lines):            
            txt_x = left
            txt_y = top - (i+1) * (line_height + self.spacer)
                
            blf.position(0, txt_x, txt_y, 0)
            bgl.glColor4f(*txt_color)
            blf.draw(0, line)

        # Draw graph
        txt_y -= 10
        graph_width = self.width
        graph_origin = Vector((left, txt_y - self.graph_height))
        y_end = Vector((graph_origin[0], txt_y))
        x_end = Vector((left+graph_width, graph_origin[1]))

        # Draw axis x, y
        if len(self.srd) > 0:
            ud.draw_polyline_from_points(context, [graph_origin, x_end], 
                txt_color, 2, 'GL_LINE_SMOOTH')

            ud.draw_polyline_from_points(context, [graph_origin, y_end], 
                txt_color, 2, 'GL_LINE_SMOOTH')
        
            x_scale = graph_width/100.0
            min_val = float(self.srd["Min"])
            max_val = float(self.srd["Max"])
            y_scale = self.graph_height/(max_val - min_val)

            pts = []
            percent = [1, 5, 25, 50, 75, 95, 99]

            for p in percent:
                key = str(p)+"%"
                value = self.srd[key] - min_val
                pts.append(graph_origin+ 
                    Vector((float(p)*x_scale, float(value)*y_scale)))

            ud.draw_polyline_from_points(context, pts, 
                (0,1,0,1), 1, 'GL_LINE_SMOOTH')

            # draw some text to x axis
            pt_O = graph_origin.copy()
            pt_O[0] -= 5
            pt_O[1] -= 12 
            ud.draw_text("0%", pt_O, txt_color)

            pt_50 = graph_origin.copy()
            pt_50[0] += x_scale*50
            pt_50[1] -= 12 
            ud.draw_text("50%", pt_50, txt_color)

            # Your position 
            result_left = context.scene.foot_measure_result_left
            result_right = context.scene.foot_measure_result_right

            if self.name == "Arch":    
                left_val = result_left.arch_center[2]
                right_val = result_right.arch_center[2]
               
            elif self.name == "Breadth":
                left_val =  result_left.x_max_pos[0] - result_left.x_min_pos[0]
                right_val = result_right.x_max_pos[0] -result_right.x_min_pos[0]
               
            elif self.name == "Length":
                left_val =  result_left.y_max_pos[1] - result_left.y_min_pos[1]
                right_val = result_right.y_max_pos[1] -result_right.y_min_pos[1]
            
            your_val  = (abs(left_val) + abs(right_val))*0.5

            start = graph_origin.copy()
            start[0] -= 10
            start[1] += (your_val - min_val)*y_scale
            end = start.copy()
            end[0] += graph_width
            color = (1,0,0,1)
            ud.draw_text("You", start - Vector((50,0)), color)
            ud.draw_polyline_from_points(context, [start, end], 
                color, 1, 'GL_LINE_STIPPLE')

            # ave value 
            start = graph_origin.copy()
            start[0] -= 10
            start[1] += (self.srd["Ave."]- min_val)*y_scale
            end = start.copy()
            end[0] += graph_width
            color = (1,1,1,1)
            ud.draw_text("Ave.", start - Vector((50,0)), color)
            ud.draw_polyline_from_points(context, [start, end], 
                color, 1, 'GL_LINE_STIPPLE')
        
    