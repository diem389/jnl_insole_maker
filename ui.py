# Author: JnL
# Last updated: 
import bmesh
from bpy.types import Panel
from . import report
from . import utils_insole_maker as utils

__version__ = '0.9b1216'


class InsoleMakeToolBar:
    bl_label = "InsoleMaker"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    _type_to_icon = {
        bmesh.types.BMVert: 'VERTEXSEL',
        bmesh.types.BMEdge: 'EDGESEL',
        bmesh.types.BMFace: 'FACESEL',
    }

    @classmethod
    def poll(cls, context):
        # obj = context.active.object
        # return (obj and obj.type == 'MESH')
        return True

    def draw(self, context):
        layout = self.layout

        scene = context.scene
        setting = scene.insole_maker_setting

        layout.label("v" + __version__+ " | facebook/JnLinc")

        ci = scene.customer
        row = layout.row()
        row.prop( ci, "customer_age")
        row.prop( ci, "customer_sex")

        layout.operator("mesh.insolemaker_measure_foot")
        if utils.is_left_foot_valid(context) or \
                utils.is_right_foot_valid(context):
            layout.operator("mesh.insolemaker_deform_insole")

            if utils.is_valid_object(context, "insole left") or \
                    utils.is_valid_object(context, "insole right"):
                layout.prop(setting, 'is_display_insole')
    

        # Dynamic view 4

        # Display result

        layout.prop(setting, "is_display_measurements")
        if utils.is_left_foot_valid(context) or \
                utils.is_right_foot_valid(context):
            

            if setting.is_display_measurements:
                layout.prop(setting, 'dimensions_kind')
           
                if setting.dimensions_kind == 'Arch':
                   # if not setting.is_display_insole:
                    layout.operator("mesh.insolemaker_adjust_arch")
                   
                    if utils.is_valid_object(context, "insole left") or \
                        utils.is_valid_object(context, "insole right"):
                        layout.prop(ci, "is_applying_srd")
                        layout.prop( ci, "srd_suggest")
                        layout.prop( ci, "compensate_ratio")

            row = layout.row()
            if utils.is_left_foot_valid(context):
                row.operator("mesh.insolemaker_result_left")

            if utils.is_right_foot_valid(context):
                row.operator("mesh.insolemaker_result_right")


        layout.operator("screen.region_quadview")
        layout.operator("view3d.ruler")


        # Display Reports
        info = report.info()

        if info:
            layout.label("Output:")

            box = layout.box()
            col = box.column(align=False)
            # print("Column number = ", col)
            # box.alert = True
            for i, text in enumerate(info):
                col.label(text)
                # print("[Debug]output:", i, text)


        # Setup for scan data UI ----
        layout.label("Setup for scan data:")
        box = layout.box()

        # Import
        box.prop(setting, "import_path", text="")
        box.operator("mesh.insolemaker_import_scan")

        # Base Plane & Cluster
        box.operator("mesh.insolemaker_find_base_plane")
        box.prop(setting, "sole_thickness")
        box.operator("mesh.insolemaker_cluster_feet")

        box.operator("mesh.insolemaker_calc_dims")

        # ---- 


# So we can have a panel in both object mode and editmode
class InsoleMakerToolBarObject(Panel, InsoleMakeToolBar):
    bl_category = "InsoleMaker"
    bl_idname = "MESH_PT_insolemaker_object"
    bl_context = "objectmode"


class InsoleMakerToolBarMesh(Panel, InsoleMakeToolBar):
    bl_category = "InsoleMaker"
    bl_idname = "MESH_PT_insolemaker_mesh"
    bl_context = "mesh_edit"
