# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Author: JnL

# Report errors with the mesh.


_data = []
_line_display = 5

def update(*args):
    print("updated-", args)
    _data[:] = args

def info():
    return tuple(_data)
    
def append(str):
    _data.append(str)
    
def log(str):
    print(str)
    _data.append(str)
    # remove lines over _line_display
    if len(_data) > _line_display:
        to_del = len(_data) - _line_display
        del _data[0:to_del]

def reset():
    for index, item in enumerate(_data):
        _data[index] = ""
        
