# Author: JnL

# <pep8-80 compliant>


bl_info = {
    "name": "Insole Maker",
    "author": "JnL",
    "blender": (2, 74, 0),
    "location": "3D View > Toolbox",
    "description": "Utilities for Insole Maker",
    "warning": "",
    "wiki_url": "",
    "support": 'COMMUNITY',
    "category": "Object"
}

if "bpy" in locals():
    import importlib

    importlib.reload(ui)
    importlib.reload(operators)
else:
    import bpy
    from bpy.props import (StringProperty,
                           BoolProperty,
                           FloatProperty,
                           FloatVectorProperty,
                           EnumProperty,
                           PointerProperty,
                           IntProperty,
                           )
    from bpy.types import (PropertyGroup,
                           )

    from . import (ui,
                   operators,
                   )


# Setting

class InsoleMakeSettings(PropertyGroup):

    import_path = StringProperty(
        name="Import Path",
        description="Path to 3D scan file",
        default="", maxlen=1024, subtype="FILE_PATH",
    )

    sole_thickness = FloatProperty(
        name="Thickness",
        description="Sole thickness",
        default=30.0, min=0, unit='LENGTH')

    plane_center_str = StringProperty()
    plane_normal_str = StringProperty()
    is_auto_mode = BoolProperty()
    is_left_display = BoolProperty(default=True)

    is_display_measurements = BoolProperty(name="Display Measurements",
        default=False, update=operators.invoke_display_measure)

    is_display_insole = BoolProperty(name="Display Insole", default=False,
        update= operators.update_display)

    dimensions_kind = EnumProperty(
        items=[('Breadth', "Breadth", "Foot breadth"),
               ('Length', "Length", "Foot length"),
               ('Arch', "Arch", "Arch info.")],
        name='Dimensions',
        default='Breadth',
        update=operators.update_dimension_kind)


    # Read-only string property, returns the current date
    # def get_date(self):
    # import datetime
    # return str(datetime.datetime.now())

    # test_date = StringProperty(get=get_date, options={'HIDDEN'})


class FootMeasureResult(PropertyGroup):
    # Measure result
    # breadth
    x_min_pos = FloatVectorProperty()
    x_max_pos = FloatVectorProperty()

    # foot length
    y_min_pos = FloatVectorProperty()
    y_max_pos = FloatVectorProperty()

    # arch region
    arch_center = FloatVectorProperty()
    arch_x_min_pos = FloatVectorProperty()
    arch_x_max_pos = FloatVectorProperty()
    arch_y_min_pos = FloatVectorProperty()
    arch_y_max_pos = FloatVectorProperty()
    arch_z_min_pos = FloatVectorProperty()
    arch_z_max_pos = FloatVectorProperty()



    def init_arch(self, vec_init):
        self.arch_center    = vec_init
        self.arch_x_min_pos = vec_init
        self.arch_x_max_pos = vec_init
        self.arch_y_min_pos = vec_init
        self.arch_y_max_pos = vec_init
        self.arch_z_min_pos = vec_init
        self.arch_z_max_pos = vec_init



class CustomerInfo(PropertyGroup):

    '''Standard Reference DB corresponding with customer age & sex'''
    customer_age = IntProperty(
        name="Age",
        description="Customer's age",
        default=30, min=10, max=79, update=operators.update_srdb)

    customer_sex = EnumProperty(items=[('Male', "Male", "Male"), 
        ('Female', "Female", "Female")], name='Sex', 
        description="Customer's sex",default='Male', 
        update=operators.update_srdb)

    srd_suggest = FloatProperty(
        name="SRD suggest", 
        description="Arch height suggested by SRD",
        default=0.0, min=0.0, unit='LENGTH', step=10,  
        update=operators.deform_arch)

    compensate_ratio = FloatProperty(
        name="Comp. ratio(%)", description="Compensation Ratio(0.0-1.0)", 
        default=0.0, min=0.0, max=1, step=10, update=operators.deform_arch)

    is_applying_srd = BoolProperty(
        name="Apply SRD to Arch", 
        description="On/Off of applying of SDR",
        default=False, update=operators.deform_arch)
    
#
# Register classes
#       
classes = (

    ui.InsoleMakerToolBarObject,
    ui.InsoleMakerToolBarMesh,

    # operators
    operators.MeasureFoot,
    operators.ImportScan,
    operators.FindBasePlane,
    operators.ClusterFeet,
    operators.ResultLeftFoot,
    operators.ResultRightFoot,
    operators.CalcDimension,

    # Display measuring result
    operators.DisplayMeasure,

    # Deform Arch
    operators.DeformInsole,
    operators.AdjustArchPos,

    # Property values 
    InsoleMakeSettings,
    FootMeasureResult,
    CustomerInfo, 
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.insole_maker_setting = \
        PointerProperty(type=InsoleMakeSettings)
    
    bpy.types.Scene.foot_measure_result_left = \
        PointerProperty(type=FootMeasureResult)
    
    bpy.types.Scene.foot_measure_result_right = \
        PointerProperty(type=FootMeasureResult)
    
    bpy.types.Scene.customer = PointerProperty(type=CustomerInfo)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.insole_maker_setting
    del bpy.types.Scene.foot_measure_result_left
    del bpy.types.Scene.foot_measure_result_right
    del bpy.types.Scene.customer


if __name__ == "__main__":
    register()
