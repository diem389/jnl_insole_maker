# Author: JnL
# <pep8-80 compliant>
# Author: JnL
# All Operator

from bpy.types import Operator
from mathutils import *
from . import report
from .measure_items import *


# Operations
# - Main Operator
class MeasureFoot(Operator):
    """Load 3D scan data and try initial alignment"""
    bl_idname = "mesh.insolemaker_measure_foot"
    bl_label = "Measure Foot"
    bl_options = {'UNDO'}

    def execute(self, context):
        # Path example
        # "C:\\Projects\\InsoleMaker\\scan_data\\foot1_150629\\0_Fused.obj"
        report.reset()

        # load related db
        update_srdb(self, context)

        setting = context.scene.insole_maker_setting
        setting.is_auto_mode = True

        bpy.ops.mesh.insolemaker_import_scan()
        bpy.ops.mesh.insolemaker_find_base_plane()
        bpy.ops.mesh.insolemaker_cluster_feet()
        bpy.ops.mesh.insolemaker_calc_dims()

        setting.is_auto_mode = False
        return {'FINISHED'}


# - Sub Operators
class ImportScan(Operator):
    """Import scan data"""
    bl_idname = "mesh.insolemaker_import_scan"
    bl_label = "Import Scan"
    bl_options = {'UNDO'}

    def execute(self, context):

        setting = context.scene.insole_maker_setting
        path = setting.import_path
        import os
        if os.path.exists(path):
            ret = utils.load_obj(path)
            if ret:
                report.log("Scan file is loaded.")
            report.log("ImportScan Finished.")
        else:
            report.log("ImportScan Failed.")

        return {'FINISHED'}


class FindBasePlane(Operator):
    """ Find Base Plane from 3D cursor position & perform initial align."""
    bl_idname = "mesh.insolemaker_find_base_plane"
    bl_label = "Find Base plane"

    def execute(self, context):

        setting = context.scene.insole_maker_setting

        if utils.is_scan_valid(context) == False:
            report.log("No Scan data.")
            report.log("- Set Scan file path.")

            return {'FINISHED'}

        if setting.is_auto_mode:
            if setting.plane_center_str == "" or \
                            setting.plane_normal_str == "":
                report.log("No Base plane data.")
                report.log("- Find Base plane first.")

                return {'FINISHED'}
            else:
                center = utils.str_to_val(setting.plane_center_str)
                normal = utils.str_to_val(setting.plane_normal_str)

        else:
            center, normal = utils.find_base_plane(context)
            # update setting
            setting.plane_center_str = str(center)
            setting.plane_normal_str = str(normal)

        utils.clip_scan_data(context, center, normal, setting.sole_thickness)
        report.log("Base plane Found.")
        return {'FINISHED'}


class ClusterFeet(Operator):
    """Cluster feet to left & right foot and align each foot"""
    bl_idname = "mesh.insolemaker_cluster_feet"
    bl_label = "Cluster feet"

    def execute(self, context):
        setting = context.scene.insole_maker_setting

        if setting.plane_center_str != "":
            left, right = utils.cluster_feet()
            report.log("Feet Clustered.")
            if left is not None:
                utils.align_foot2(left)
                report.log("Left foot aligned")

            if right is not None:
                utils.align_foot2(right)
                report.log("Right foot aligned")


            bpy.ops.mesh.insolemaker_result_left()

        else:
            report.log("No Base plane data")
        return {'FINISHED'}


class CalcDimension(Operator):
    """Get dimension"""
    bl_idname = "mesh.insolemaker_calc_dims"
    bl_label = "Calc. dimension"

    def execute(self, context):
        setting = context.scene.insole_maker_setting

        left = utils.get_left_foot(context)
        right = utils.get_right_foot(context)

        if left is not None and right is not None:
            measure_result_left = context.scene.foot_measure_result_left
            measure_result_right = context.scene.foot_measure_result_right


            utils.get_dim_pos_and_fill_result(left, measure_result_left)
            utils.get_arch_region_info_and_fill_result(left,
                                                       measure_result_left)


            utils.get_dim_pos_and_fill_result(right, measure_result_right)
            utils.get_arch_region_info_and_fill_result(right,
                                                       measure_result_right)

            setting.is_display_measurements = True
            bpy.ops.mesh.insolemaker_result_left()

        return {'FINISHED'}



class ShowMeasuredFoot:
    def show_measured(self, context, obj_measured, measured):

        # Display dimensions to log
        di = obj_measured.dimensions

        report.reset()
        report.log("* " + obj_measured.name)
        report.log("-Length: %.2f mm" % di[1])  # y axis
        report.log("-Breadth: %.2f mm" % di[0])  # x axis

        # Display on the screen
        name = context.scene.insole_maker_setting.dimensions_kind
        if name == "Breadth":
            p1 = Vector(measured.x_min_pos)
            p2 = Vector(measured.x_max_pos)
            set_display_item(Length(name, p1, p2))

        elif name == "Length":
            p1 = Vector(measured.y_min_pos)
            p2 = Vector(measured.y_max_pos)
            set_display_item(Length(name, p1, p2))
        elif name == "Arch":
            set_display_item(ArchInfo(measured))
        else:
            # some error
            print("[Error] Unknown dimension name")
            return

        # Standard Reference Data Display
        add_display_item(SrDisplay(name))

    def show_valid(self, context):

        foot_name = "foot " + self.bl_label.lower()
        foot_obj = context.scene.objects.get(foot_name)
        if foot_obj is None:
            report.log("No foot data.")
            return {'CANCELLED'}

        if self.bl_label.lower() == "left":
            measured = context.scene.foot_measure_result_left
        else:
            measured = context.scene.foot_measure_result_right

        self.show_measured(context, foot_obj, measured)

        # change display
        # Hide all
        utils.hide_all(context)
        #
        insole_name = "insole " + self.bl_label.lower()
        insole_obj = context.scene.objects.get(insole_name)

        setting = context.scene.insole_maker_setting

        if insole_obj is not None and setting.is_display_insole:
            insole_obj.hide = False
            insole_obj.select = True
            context.scene.objects.active = insole_obj
            utils.zoom_and_view('BOTTOM')
        else:
            foot_obj.hide = False
            foot_obj.select = True
            context.scene.objects.active = foot_obj
            utils.zoom_and_view('TOP')

        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')

        print("[Debug] show_valid called.")
        return {'FINISHED'}


class ResultLeftFoot(ShowMeasuredFoot, Operator):
    """Show left foot's dimension"""
    bl_idname = "mesh.insolemaker_result_left"
    bl_label = "Left"

    def execute(self, context):
        context.scene.insole_maker_setting.is_left_display = True
        return self.show_valid(context)


class ResultRightFoot(ShowMeasuredFoot, Operator):
    """Show Right foot's dimension"""
    bl_idname = "mesh.insolemaker_result_right"
    bl_label = "Right"

    def execute(self, context):
        context.scene.insole_maker_setting.is_left_display = False
        return self.show_valid(context)


class DisplayMeasure(Operator):
    bl_idname = "mesh.insolemaker_display_measure"
    bl_label = "Display Measurements"
    bl_description = "Display the measurements made in the InsoleMaker"

    def __init__(self):
        self._handle = None

    def reset_draw_handle(self):
        if self._handle is not None:
            bpy.types.SpaceView3D.draw_handler_remove(
                    self._handle, 'WINDOW')
            self._handle = None

    def modal(self, context, event):
        context.area.tag_redraw()

        if event.type == 'MOUSEMOVE':
            pass

        elif event.type == 'LEFTMOUSE':
            pass

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            context.scene.insole_maker_setting.is_display_measurements = False
            return {'CANCELLED'}

        return {'PASS_THROUGH'}
        # return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        print("debug: draw invoke called")
        # check value
        if not context.scene.insole_maker_setting.is_display_measurements:
            # Cancel previous operation
            print("debug: invoke stop draw")
            self.reset_draw_handle()
            return {'CANCELLED'}

        if context.area.type == 'VIEW_3D':
            args = (self, context)
            self._handle = bpy.types.SpaceView3D.draw_handler_add(
                    draw_handler, args, 'WINDOW', 'POST_PIXEL')
            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}


class DeformInsole(Operator):
    """ Generate Customized Insole Model"""
    bl_idname = "mesh.insolemaker_deform_insole"
    bl_label = "Generate Insole"

    def execute(self, context):
        # remove prev. insole
        utils.remove_all_meshes_except(foot=True)
        tag_lr = ["left", "right"]

        for tag in tag_lr:

            if not utils.is_valid_object(context, "foot " + tag):
                report.log("[Warning] No {} foot data... ".format(tag))
                return {'CANCELLED'}

            if not utils.is_valid_object(context, "ref insole " + tag):
                report.log("[Warning] No {} ref insole data ...".format(tag))
                return {'CANCELLED'}

            scene = context.scene
            limit = scene['foot_measure_result_%s' % (tag,)]['arch_center'][
                        2] * 2.
            limit = abs(limit)
            print("[Debug] %s insole arch limit: " % (tag,), limit)

            utils.deform_insole(context, tag, limit)

        # result
        setting = context.scene.insole_maker_setting
        setting.is_display_insole = True
        bpy.ops.mesh.insolemaker_result_left()
        return {'FINISHED'}


class AdjustArchPos(Operator):
    """ Update arch info based from the current 3D cursor position"""
    bl_idname = "mesh.insolemaker_adjust_arch"
    bl_label = "Adjust Arch Pos"
    bl_options = {'UNDO'}

    def execute(self, context):
        print("[Debug] AdjustArchPos Called")

        # Get the current scene
        scene = context.scene
        cursor = scene.cursor_location.copy()
        setting = scene.insole_maker_setting

        if setting.is_left_display:
            scene.foot_measure_result_left.init_arch(cursor)
        else:
            scene.foot_measure_result_right.init_arch(cursor)

        return {'FINISHED'}


#
# Global functions for update
#
def invoke_display_measure(self, context):
    from .utils_insole_maker import run_operator_on_view3d, get_time_stamp
    print("%s Debug, invoke_display_measure called..." % (get_time_stamp()))
    run_operator_on_view3d(bpy.ops.mesh.insolemaker_display_measure)
    return None


def update_display(self, context):
    # result
    if context.scene.insole_maker_setting.is_left_display:
        bpy.ops.mesh.insolemaker_result_left()
    else:
        bpy.ops.mesh.insolemaker_result_right()

    return None


def update_dimension_kind(self, context):
    return update_display(self, context)


def update_srdb(self, context):
    from . import kriss_api as ka

    ci = context.scene.customer
    if ci.customer_sex == 'Male':
        sex_code = 1
    else:
        sex_code = 2

    db_breadth, db_length, db_arch = \
        ka.get_std_ref(int(ci.customer_age), sex_code,
                       debug=True)
    # Save to group
    group = bpy.data.groups.get("SRDB")
    if group is None:
        group = bpy.data.groups.new("SRDB")
        print("[Debug] group creation called... ")

    group["Breadth"] = db_breadth
    group["Length"] = db_length
    group["Arch"] = db_arch

    srd_suggest = db_arch.get("Ave.")
    if srd_suggest is not None:
        ci.srd_suggest = srd_suggest

    # ci.compensate_ratio = 0.0 // Init

    # Redraw display
    update_dimension_kind(self, context)
    return None


def draw_handler(self, context):
    # print("[Debug] display handler called")

    setting = context.scene.insole_maker_setting
    if not setting.is_display_measurements:
        return

    display_list = get_display_items()
    for m in display_list:
        m.display(context)


def deform_arch(self, context):
    # print("deform_arch, ", self, context)
    setting = context.scene.insole_maker_setting
    ci = context.scene.customer

    if not ci.is_applying_srd:
        return None

    if setting.is_left_display:

        left_arch = context.scene.foot_measure_result_left.arch_center
        # Change user arch info

        utils.deform_insole_with_new_arch(context, "left", left_arch,
                                          ci.srd_suggest, ci.compensate_ratio)

        left_arch[2] = -ci.srd_suggest

    else:
        right_arch = context.scene.foot_measure_result_right.arch_center
        # Change user arch info

        utils.deform_insole_with_new_arch(context, "right", right_arch,
                                          ci.srd_suggest, ci.compensate_ratio)

        right_arch[2] = -ci.srd_suggest

    return None
