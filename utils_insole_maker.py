# Author: JnL

import numpy as np

import bpy
import bmesh
from mathutils import *

_base_plane_center = None
_base_plane_normal = None

_left_foot_name = "foot left"
_left_ref_insole_name = "ref insole left"
_left_insole_name = "insole left"

_right_foot_name = "foot right"
_right_ref_insole_name = "ref insole right"
_right_insole_name = "insole right"

_verts_for_lattice_name = "Verts for lattice"
_lattice_name = "-lattice"


# copied from mesh_helpers.py
def bmesh_copy_from_object(obj, transform=True, triangulate=True,
                           apply_modifiers=False):
    """
    Returns a transformed, triangulated copy of the mesh
    """

    assert (obj.type == 'MESH')

    if apply_modifiers and obj.modifiers:
        import bpy
        me = obj.to_mesh(bpy.context.scene, True, 'PREVIEW',
                         calc_tessface=False)
        bm = bmesh.new()
        bm.from_mesh(me)
        bpy.data.meshes.remove(me)
        del bpy
    else:
        me = obj.data
        if obj.mode == 'EDIT':
            bm_orig = bmesh.from_edit_mesh(me)
            bm = bm_orig.copy()
        else:
            bm = bmesh.new()
            bm.from_mesh(me)

    if transform:
        bm.transform(obj.matrix_world)

    if triangulate:
        bmesh.ops.triangulate(bm, faces=bm.faces)

    return bm


def bmesh_from_object(obj):
    """
    Object/Edit Mode get mesh, use bmesh_to_object() to write back.
    """
    me = obj.data
    is_editmode = (obj.mode == 'EDIT')
    if is_editmode:
        bm = bmesh.from_edit_mesh(me)
    else:
        bm = bmesh.new()
        bm.from_mesh(me)
    return bm


def bmesh_to_object(obj, bm):
    """
    Object/Edit Mode update the object.
    """
    me = obj.data
    is_editmode = (obj.mode == 'EDIT')
    if is_editmode:
        bmesh.update_edit_mesh(me, True)
    else:
        bm.to_mesh(me)
    # grr... cause an update
    if me.vertices:
        me.vertices[0].co[0] = me.vertices[0].co[0]


def bmesh_face_points_random(f, num_points=1, margin=0.05):
    import random
    from random import uniform
    uniform_args = 0.0 + margin, 1.0 - margin

    # for pradictable results
    random.seed(f.index)

    vecs = [v.co for v in f.verts]

    for i in range(num_points):
        u1 = uniform(*uniform_args)
        u2 = uniform(*uniform_args)
        u_tot = u1 + u2

        if u_tot > 1.0:
            u1 = 1.0 - u1
            u2 = 1.0 - u2

        side1 = vecs[1] - vecs[0]
        side2 = vecs[2] - vecs[0]

        yield vecs[0] + u1 * side1 + u2 * side2


def str_to_val(s):
    import ast
    print("[Debug]" + s)
    return ast.literal_eval(s)


def remove_all_meshes():
    # show objects
    objects = bpy.context.scene.objects
    for ob in objects:
        ob.hide = False

    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_by_type(type='MESH')
    bpy.ops.object.delete(use_global=False)

    for item in bpy.data.meshes:
        bpy.data.meshes.remove(item)


def remove_all_meshes2():
    objects = bpy.context.scene.objects
    for ob in objects:
        ob.hide = False

    # Deselect everything
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

    # gather list of items of interest.
    candidate_list = [item.name for item in bpy.data.objects
                      if item.type == "MESH"]

    # select them only.
    for object_name in candidate_list:
        bpy.data.objects[object_name].select = True

    # remove all selected.
    bpy.ops.object.delete()

    # remove the meshes, they have no users anymore.
    for item in bpy.data.meshes:
        bpy.data.meshes.remove(item)


def remove_all_meshes_except(foot=False, ref=True):
    """ Remove mesh except name start with 'ref' string
    :return:
    """
    objects = bpy.context.scene.objects
    for ob in objects:

        if ref:
            if ob.name[:3] == "ref":
                ob.hide = True
            else:
                ob.hide = False

    #
    # # De-select everything
    if bpy.context.mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    #
    # # gather list of items of interest.
    candidate_list = []
    for item in bpy.data.objects:
        if item.type == "MESH" or item.type == "LATTICE":

            if ref and item.name[:3] == "ref":
                continue

            if foot and item.name[:4] == "foot":
                continue

            candidate_list.append(item)

    # select them only.
    for ob in candidate_list:
        ob.select = True
    #
    # # remove all selected.
    bpy.ops.object.delete()

    # # remove the meshes, they have no users anymore.
    # for item in bpy.data.meshes:
    #     if item.name[:3] != "ref":
    #         bpy.data.meshes.remove(item)


def hide_all(context):
    ob_list = []
    for item in bpy.data.objects:
        if item.type == "MESH" or item.type == "LATTICE":
            ob_list.append(item)

    # select them only
    for ob in ob_list:
        ob.select = False
        ob.hide = True


def zoom_and_view(type_='TOP'):
    run_operator_on_view3d(bpy.ops.view3d.viewnumpad,
                           type=type_, align_active=True)
    bpy.context.scene.update()
    run_operator_on_view3d(bpy.ops.view3d.view_selected)
    bpy.context.scene.update()


def print_vertex():
    for item in bpy.data.objects:
        if item.type == 'MESH':
            print(item.name)
            for vertex in item.data.vertices:
                print(vertex.co)


def find_seed_vtx(context):
    """ KDtree Test"""
    obj = context.object

    # 3d cursor relative to the object data
    co_find = context.scene.cursor_location * obj.matrix_world.inverted()

    mesh = obj.data
    size = len(mesh.vertices)
    kd = kdtree.KDTree(size)

    for i, v in enumerate(mesh.vertices):
        kd.insert(v.co, i)

    kd.balance()

    # Find the closest point to the center
    co, index, dist = kd.find(co_find)
    print("Close to center:", co, index, dist)

    return index, co, dist


def plane_fit(points):
    """
    p, n = plane_fit(points)

    Given an array, points, of shape (d,...)
    representing points in d-dimensional space,
    fit an d-dimensional plane to the points.
    Return a point, p, on the plane (the point-cloud centroid),
    and the normal, n.
    """
    from numpy.linalg import svd
    # Collapse trailing dimensions
    points = np.reshape(points, (np.shape(points)[0], -1))
    assert points.shape[0] <= points.shape[1], \
        "There are only {} points in {} dimensions.".format(points.shape[1],
                                                            points.shape[0])
    ctr = points.mean(axis=1)
    x = points - ctr[:, np.newaxis]

    M = np.dot(x, x.T)  # Could also use cov(x) here.
    return tuple(ctr), tuple(svd(M)[0][:, -1])  # return pt, normal


def dist_to_plane(points, plane_center, plane_normal):
    """ absolute value of distance """
    return abs(np.dot(points - plane_center, plane_normal))


def fit_selection_to_plane(context):
    me = context.object.data

    selected = [v for v in me.vertices if v.select == True]

    vertices_coords = np.zeros((3, len(selected)))

    for i, v in enumerate(selected):
        vertices_coords[0, i] = v.co.x
        vertices_coords[1, i] = v.co.y
        vertices_coords[2, i] = v.co.z

    return plane_fit(vertices_coords)


def select_by_plane(context, ptr, normal, t=10, inverse=False):
    me = context.object.data

    selected_value = not (inverse)

    for v in me.vertices:
        if dist_to_plane(np.array(v.co), ptr, normal) < t:
            v.select = selected_value
        else:
            v.select = not (selected_value)


def select_seed_vtx(context):
    """ KDtree Test"""

    obj = context.object

    # 3d cursor relative to the object data
    co_find = context.scene.cursor_location * obj.matrix_world.inverted()

    mesh = obj.data
    size = len(mesh.vertices)
    kd = kdtree.KDTree(size)

    for i, v in enumerate(mesh.vertices):
        kd.insert(v.co, i)

    kd.balance()

    # Find the closest point to the center
    co, index, dist = kd.find(co_find)
    print("Close to center:", co, index, dist)

    check = False
    for v in mesh.vertices:
        if v.index == index:
            v.select = True
            print("verrtex selected")
            check = True
        else:
            v.select = False

    if check == False:
        print("vertex is not selected.")


# General Utils
def print_current_time():
    from time import gmtime, strftime
    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))


def clean_float(text):
    # strip trailing zeros: 0.000 -> 0.0
    index = text.rfind(".")
    if index != -1:
        index += 2
        head, tail = text[:index], text[index:]
        tail = tail.rstrip("0")
        text = head + tail
    return text


_default_obj_path = \
    "C:\\Projects\\InsoleMaker\\scan_data\\foot1_150629\\0_Fused.obj"


def load_obj(path=_default_obj_path, not_transform=True):
    remove_all_meshes_except()
    context = bpy.context
    bpy.ops.import_scene.obj(filepath=path)
    feet_org = context.selected_objects[0]

    scn = context.scene

    feet_org.select = True
    scn.objects.active = feet_org

    if not_transform:
        feet_org.matrix_world = Matrix()

    zoom_and_view()
    return True


# 2015.8.

def select_ankle(context, sole_thickness=30):
    pass


def find_base_plane(context, save_vertices_coord=False):
    """
    center, normal = find_base_plane(C)

    guess base plane from 3D Cursor pos.
    """

    # Start from object mode
    # change mode to edit
    bpy.ops.object.mode_set(mode='EDIT')

    # init. selection
    bpy.ops.mesh.select_all(action='DESELECT')

    obj = context.object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    co_find = context.scene.cursor_location * obj.matrix_world.inverted()

    kd = kdtree.KDTree(len(bm.verts))
    for i, vert in enumerate(bm.verts):
        kd.insert(vert.co, i)

    kd.balance()

    co, index, dist = kd.find(co_find)
    print(co, index, dist)
    if index == -1:
        print("KDTree finding fail.")
        return
    else:
        print("Found index = ", index)

    bm.verts[index].select = True

    # expand the selection five times
    # change selection mode to vertex selection
    context.tool_settings.mesh_select_mode = (True, False, False)
    selection_iter = 5
    for iter in range(selection_iter):
        bpy.ops.mesh.select_more()

    vlist = [v for v in bm.verts if v.select == True]
    print("selected vertices num. = ", len(vlist))

    vertices_coords = np.zeros((3, len(vlist)))
    for i, v in enumerate(vlist):
        print(v.co)
        vertices_coords[0, i] = v.co.x
        vertices_coords[1, i] = v.co.y
        vertices_coords[2, i] = v.co.z

    if save_vertices_coord:
        import os
        f1 = os.path.join(os.path.dirname(__file__), 'base_plane_vertices.npy')
        print("vertices_coord file path:", f1)
        np.save(f1, vertices_coords)

    center, normal = plane_fit(vertices_coords)
    print("base plane center = ", center)
    print("base plane normal = ", normal)

    # clear
    bm.free()

    # change mode to object
    bpy.ops.object.mode_set(mode='OBJECT')

    return center, normal


def clip_scan_data(context, center, normal, thickness=30):
    """ clip volume by height
    """
    # Start from object mode
    # change mode to edit
    mode_orig = context.mode

    bpy.ops.object.mode_set(mode='EDIT')

    # init. selection
    bpy.ops.mesh.select_all(action='DESELECT')

    obj = context.object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()

    t = thickness  # mm
    selected_value = False
    not_selected_value = not (selected_value)
    # count = 0
    for v in bm.verts:
        dist = dist_to_plane(np.array(v.co), center, normal)
        if dist < t:
            # v.select = selected_value
            v.select_set(selected_value)
            # count = count + 1
            # print("index = %d, dist = %f, vertex selected..."%(count, dist))
        else:
            # v.select = not_selected_value
            v.select_set(not_selected_value)

    bmesh.update_edit_mesh(me)
    bpy.ops.mesh.delete(type='VERT')
    bm.free()

    # change mode to object
    bpy.ops.object.mode_set(mode='OBJECT')
    align_to_plane(obj, center, normal)


def get_left_foot(context):
    return context.scene.objects.get(_left_foot_name)


def get_left_insole(context):
    return context.scene.objects.get(_left_insole_name)


def get_left_ref_insole(context):
    return context.scene.objects.get(_left_ref_insole_name)


def get_right_foot(context):
    return context.scene.objects.get(_right_foot_name)


def get_right_insole(context):
    return context.scene.objects.get(_right_insole_name)


def get_right_ref_insole(context):
    return context.scene.objects.get(_right_ref_insole_name)


def is_left_foot_valid(context):
    return _left_foot_name in context.scene.objects


def is_right_foot_valid(context):
    return _right_foot_name in context.scene.objects


def is_valid_object(context, name):
    return name in context.scene.objects


def is_scan_valid(context):
    candidate_list = [item.name for item in bpy.data.objects
                      if item.type == "MESH"]
    return len(candidate_list) > 0


def align_to_plane(obj, plane_center, plane_normal):
    mat = Matrix()

    zaxis = Vector(plane_normal)
    zaxis.normalize()
    xaxis = Vector((1, 0, 0))
    yaxis = Vector((0, 1, 0))

    if xaxis.dot(zaxis) > 0.9:
        xaxis = yaxis.cross(zaxis)
        yaxis = zaxis.cross(xaxis)
    else:
        yaxis = zaxis.cross(xaxis)
        xaxis = yaxis.cross(zaxis)

    xaxis.normalize()
    yaxis.normalize()
    zaxis.normalize()

    mat[0].xyz = xaxis
    mat[1].xyz = yaxis
    mat[2].xyz = zaxis
    mat[3].xyz = Vector(plane_center)

    mat.transpose()

    obj.matrix_world = mat.inverted()
    bpy.context.scene.objects.active = obj
    bpy.context.scene.update()

    bpy.ops.object.transform_apply(location=True,
                                   rotation=True, scale=True)

    zoom_and_view()


def calc_mesh_properties(me):
    """
        calc center, x's min/max, y's min/max, z's min/max
        return cx, cy, cz, xmin, xmax, ymin, ymax, zmin, zmax
    """

    bm = bmesh.new()
    bm.from_mesh(me)

    import sys
    xmax = -sys.float_info.max
    xmin = sys.float_info.max
    ymax = -sys.float_info.max
    ymin = sys.float_info.max
    zmax = -sys.float_info.max
    zmin = sys.float_info.max

    cx = cy = cz = 0.0

    for v in bm.verts:

        cx += v.co[0]
        cy += v.co[1]
        cz += v.co[2]

        if v.co[0] > xmax:
            xmax = v.co[0]
        if v.co[0] < xmin:
            xmin = v.co[0]
        if v.co[1] > ymax:
            ymax = v.co[1]
        if v.co[1] < ymin:
            ymin = v.co[1]
        if v.co[2] > zmax:
            zmax = v.co[2]
        if v.co[2] < zmin:
            zmin = v.co[2]

    cx = cx / len(bm.verts)
    cy = cy / len(bm.verts)
    cz = cz / len(bm.verts)

    bm.free()

    return cx, cy, cz, xmin, xmax, ymin, ymax, zmin, zmax


def cluster_feet(filter_out_vtx=500):
    """
        separate right, left foot
        In display, right foot is in the left side one
        Assumption: seed point is origin point. the nearest point is a point
        on the right upper
    """
    bpy.ops.mesh.separate(type='LOOSE')

    # deselect all
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')

    # filter out noisy objects
    foot_candidates = []
    foot_distacnes_from_origin = []
    origin = Vector((0, 0, 0))
    objects = bpy.context.scene.objects
    for ob in objects:
        if ob.type == 'MESH':
            if len(ob.data.vertices) < filter_out_vtx:
                ob.select = True
                bpy.ops.object.delete()
            else:
                props = calc_mesh_properties(ob.data)
                center = Vector((props[0], props[1], props[2]))
                dist = (center - origin).length
                foot_candidates.append(ob)
                foot_distacnes_from_origin.append(dist)

    if len(foot_candidates) < 2:
        print("[Error] Something is wrong!")
        return None, None

    if foot_distacnes_from_origin[0] < foot_distacnes_from_origin[1]:
        foot_candidates[0].name = _right_foot_name
        foot_candidates[1].name = _left_foot_name
    else:
        foot_candidates[0].name = _left_foot_name
        foot_candidates[1].name = _right_foot_name

    for obj in foot_candidates:
        obj.data.name = obj.name + " mesh"

    return objects[_left_foot_name], objects[_right_foot_name]


def computer_principal_axes(coord):
    """
        input parameter : xys
        [[x1 y1 z1], [x2 y2 z2], [.. .. ..], [xn yn zn]]
    """

    # create coordinates array
    # coord = np.array(xyz, float)
    # coord = xyz

    # compute geometric center
    center = np.mean(coord, 0)
    # print "Coordinates of the geometric center:\n", center

    # center with geometric center
    coord = coord - center

    # compute principal axis matrix
    inertia = np.dot(coord.transpose(), coord)
    e_values, e_vectors = np.linalg.eig(inertia)
    # warning eigen values are not necessary ordered!
    # http://docs.scipy.org/doc/numpy/reference/generated/np.linalg.eig.html
    # print "(Unordered) eigen values:"
    # print e_values
    # print "(Unordered) eigen vectors:"
    # print e_vectors

    # --------------------------------------------------------------------------
    # order eigen values (and eigen vectors)
    #
    # axis1 is the principal axis with the biggest eigen value (eval1)
    # axis2 is the principal axis with the second biggest eigen value (eval2)
    # axis3 is the principal axis with the smallest eigen value (eval3)
    # --------------------------------------------------------------------------
    for i in range(len(e_values)):
        # find biggest eigen value
        if e_values[i] == max(e_values):
            eval1 = e_values[i]
            axis1 = e_vectors[:, i]
        # find smallest eigen value
        elif e_values[i] == min(e_values):
            eval3 = e_values[i]
            axis3 = e_vectors[:, i]
        # middle eigen value
        else:
            eval2 = e_values[i]
            axis2 = e_vectors[:, i]

    return axis1, axis2, axis3, center


def align_foot(obj, apply_ransac=True):
    # find principal axis

    bm = bmesh.new()
    bm.from_mesh(obj.data)

    xyz = np.zeros((len(bm.verts), 3), dtype=np.float)
    for i, v in enumerate(bm.verts):
        xyz[i, 0] = v.co[0]
        xyz[i, 1] = v.co[1]
        xyz[i, 2] = v.co[2]

    dist_filter_out = 2.5  # mm
    if apply_ransac:

        from . import ransac
        pt, normal, inliers = ransac.ransac_fit2plane(xyz, dist=dist_filter_out)
        xyz2 = np.zeros((len(inliers), 3), dtype=np.float)
        for j, i in enumerate(inliers):
            xyz2[j, :] = xyz[i, :]
        xyz = xyz2

    axis1, axis2, axis3, center = computer_principal_axes(xyz)
    del xyz

    xaxis = Vector((axis2[0], axis2[1], axis2[2]))
    yaxis = Vector((axis1[0], axis1[1], axis1[2]))
    zaxis = xaxis.cross(yaxis)
    origin = Vector((center[0], center[1], center[2]))

    if zaxis.z < 0:
        xaxis = -xaxis
        zaxis = -zaxis

    mat = Matrix()

    xaxis.normalize()
    yaxis.normalize()
    zaxis.normalize()

    mat[0].xyz = xaxis
    mat[1].xyz = yaxis
    mat[2].xyz = zaxis
    mat[3].xyz = origin

    mat.transpose()
    # Check y axis vector
    # origin = Vector((0, 1, 0))
    mat.invert()
    bm.transform(mat)
    bm.to_mesh(obj.data)
    bm.free()

    bpy.context.scene.objects.active = obj
    obj.select = True



def align_foot2(obj):
    """ algorithm
        1. Find a more accurate base plane with ransac, plane2
        2. Project all points to the plane2
        3. Find medial axis and compare diction of y with a vector
            - The vector is made by origin and center of mass

    """

    # 1. Find a more accurate base plane with ransac, plane2

    bm = bmesh.new()
    bm.from_mesh(obj.data)

    xyz = np.zeros((len(bm.verts), 3), dtype=np.float)
    for i, v in enumerate(bm.verts):
        xyz[i, 0] = v.co[0]
        xyz[i, 1] = v.co[1]
        xyz[i, 2] = v.co[2]

    dist_filter_out = 2.5  # mm
    from . import ransac
    pt, normal, inners = ransac.ransac_fit2plane(xyz, dist=dist_filter_out)

    # 2. Project all points to the plane2
    for v in xyz:
        vec2pt = v - pt
        distance = vec2pt.dot(normal)
        v -= distance*normal


    axis1, axis2, axis3, center = computer_principal_axes(xyz)
    del xyz

    # 3. Guess front vector
    pseudo_y = Vector((-center[0], -center[1], -center[2]))
    pseudo_y.normalize()

    xaxis = Vector((axis2[0], axis2[1], axis2[2]))
    yaxis = Vector((axis1[0], axis1[1], axis1[2]))
    zaxis = xaxis.cross(yaxis)
    origin = Vector((center[0], center[1], center[2]))

    if zaxis.z < 0:
        xaxis = -xaxis
        zaxis = -zaxis

    mat = Matrix()

    xaxis.normalize()
    yaxis.normalize()
    zaxis.normalize()

    # 4. compare with pseudo y

    if yaxis.dot(pseudo_y) < 0:
        yaxis = -yaxis
        xaxis = -xaxis

    mat[0].xyz = xaxis
    mat[1].xyz = yaxis
    mat[2].xyz = zaxis
    mat[3].xyz = origin

    mat.transpose()
    # Check y axis vector
    # origin = Vector((0, 1, 0))
    mat.invert()
    bm.transform(mat)
    bm.to_mesh(obj.data)
    bm.free()

    bpy.context.scene.objects.active = obj
    obj.select = True



def test_ransac_fit2plane(context):
    """ Test Ransac fit to plane"""
    import ransac
    obj = context.object

    bm = bmesh.new()
    bm.from_mesh(obj.data)

    xyz = np.zeros((len(bm.verts), 3), dtype=np.float)
    for i, v in enumerate(bm.verts):
        xyz[i, 0] = v.co[0]
        xyz[i, 1] = v.co[1]
        xyz[i, 2] = v.co[2]

    pt, normal = ransac.ransac_fit2plane(xyz)
    bm.free()
    align_to_plane(obj, pt, normal)
    print(pt)
    print(normal)


# Returns a tuple describing the current measuring system
# and formatting options.
# Returned data is meant to be passed to formatDistance().
# Original by Alessandro Sala (Feb, 12th 2012)
# Update by Alessandro Sala (Dec, 18th 2012)
def get_units_info():
    scale = bpy.context.scene.unit_settings.scale_length
    unit_system = bpy.context.scene.unit_settings.system
    separate_units = bpy.context.scene.unit_settings.use_separate
    if unit_system == 'METRIC':
        scale_steps = ((1000, 'km'), (1, 'm'), (1 / 100, 'cm'),
                       (1 / 1000, 'mm'), (1 / 1000000, '\u00b5m'))
    elif unit_system == 'IMPERIAL':
        scale_steps = ((5280, 'mi'), (1, '\''),
                       (1 / 12, '"'), (1 / 12000, 'thou'))
        scale /= 0.3048  # BU to feet
    else:
        scale_steps = ((1, ' BU'),)
        separate_units = False

    return (scale, scale_steps, separate_units)


# Converts a distance from BU into the measuring system
# described by units_info.
# Original by Alessandro Sala (Feb, 12th 2012)
# Update by Alessandro Sala (Dec, 18th 2012)
def convert_distance(val, units_info):
    # Precicion for display of float values.
    PRECISION = 3
    scale, scale_steps, separate_units = units_info
    sval = val * scale
    idx = 0
    while idx < len(scale_steps) - 1:
        if sval >= scale_steps[idx][0]:
            break
        idx += 1
    factor, suffix = scale_steps[idx]
    sval /= factor
    if not separate_units or idx == len(scale_steps) - 1:
        dval = str(round(sval, PRECISION)) + suffix
    else:
        ival = int(sval)
        dval = str(round(ival, PRECISION)) + suffix
        fval = sval - ival
        idx += 1
        while idx < len(scale_steps):
            fval *= scale_steps[idx - 1][0] / scale_steps[idx][0]
            if fval >= 1:
                dval += ' ' \
                        + ("%.1f" % fval) \
                        + scale_steps[idx][1]
                break
            idx += 1

    return dval


def get_dim_pos(obj, with_center=False):
    bm = bmesh_from_object(obj)
    import sys

    # x - direction
    x_min_pos = None
    x_max_pos = None
    x_min = sys.float_info.max
    x_max = -x_min

    # y - direction
    y_min_pos = None
    y_max_pos = None
    y_min = sys.float_info.max
    y_max = -x_min

    # z - direction
    z_min_pos = None
    z_max_pos = None
    z_min = sys.float_info.max
    z_max = -z_min

    center = Vector()

    for v in bm.verts:
        center += v.co;

        if v.co[0] > x_max:
            x_max = v.co[0]
            x_max_pos = v.co.copy()

        if v.co[0] < x_min:
            x_min = v.co[0]
            x_min_pos = v.co.copy()

        if v.co[1] > y_max:
            y_max = v.co[1]
            y_max_pos = v.co.copy()

        if v.co[1] < y_min:
            y_min = v.co[1]
            y_min_pos = v.co.copy()


        if v.co[1] > z_max:
            z_max = v.co[1]
            z_max_pos = v.co.copy()

        if v.co[1] < z_min:
            z_min = v.co[1]
            z_min_pos = v.co.copy()

    center /= len(bm.verts)
    bm.free()

    if with_center:
        return x_min_pos, x_max_pos, y_min_pos, y_max_pos, \
               z_min_pos, z_max_pos, center
    else:
        return x_min_pos, x_max_pos, y_min_pos, y_max_pos, \
               z_min_pos, z_max_pos


def get_dim_pos_and_fill_result(obj, result):
    """  compute min, max position along x, y, z
        direction, deciding the dimension of object and fill result
    :param obj: mesh
    :return: boolean, success or not
    """
    result.x_min_pos, result.x_max_pos, result.y_min_pos, \
    result.y_max_pos, z_min, z_max = get_dim_pos(obj)
    return True


def mesh_to_new_object(obj_name, bm):
    scene = bpy.context.scene
    me_new = bpy.data.meshes.new(name=obj_name)
    bm.to_mesh(me_new)

    obj_new = bpy.data.objects.new(name=me_new.name, object_data=me_new)
    scene.objects.link(obj_new)
    scene.update()


def test_get_arch_region(obj):

    import datetime
    now = datetime.datetime.now()
    log_file_name = "jnl_log_" + now.strftime("%y%m%d-%H%M%S") + ".txt"
    f = open(log_file_name, 'w')

    bm = bmesh_copy_from_object(obj)
    result = bmesh.ops.convex_hull(bm, input=bm.verts, use_existing_faces=False)

    inner_list = result['geom_interior']
    hull_list = result['geom']
    del result

    not_arch_vertices = [e for e in hull_list
                          if type(e) is bmesh.types.BMVert]
    inner_vertices = [e for e in inner_list if type(e) is bmesh.types.BMVert]

    # print(ret)
    # mesh_to_new_object("inner", result['geom_interior'])
    # mesh_to_new_object("hull", result['geom'])

    me_arch_candidate = bpy.data.meshes.new(name="_tmp_for_arch")
    bm.to_mesh(me_arch_candidate)
    obj_new = bpy.data.objects.new(name=me_arch_candidate.name,
                                   object_data=me_arch_candidate)

    # Add object to Scnee
    scene = bpy.context.scene
    scene.objects.link(obj_new)
    scene.objects.active = obj_new
    obj_new.select = True
    scene.update()

    ray_cast = obj_new.ray_cast
    EPS_BIAS = 0.000001
    thickness = 5.0 # mm

    for v in inner_vertices:
        no = v.normal
        no_sta = no * EPS_BIAS
        no_end = no * thickness

        p_a = v.co + no_sta
        p_b = v.co + no_end

        result_tmp, co2, no2, index = ray_cast(p_a, p_b)

        # if we found thinner, it means convex vertices
        if index != -1:
            dist = (co2 - p_a).length
            if dist < thickness:
                not_arch_vertices.append(v)
            # v.select_set(False)
        else:
            # v.select_set(True)
            pass


    f.write("Number of not arch: " + str(len(not_arch_vertices)))
    f.write("\n")
    length = len(not_arch_vertices)
    for i in range(length):
        e = not_arch_vertices[i]
        f.write(str(e.co.x) + ", " + str(e.co.y) + ", " + str(e.co.z))
        f.write("\n")

    bmesh.ops.delete(bm, geom=not_arch_vertices, context=6)
    bmesh_to_object(obj_new, bm)
    scene.update()

    bm.free()

    # with open('inner_list.txt', 'w') as f:
    #     for item in inner_list:
    #         f.write(item.__repr__() + "\n")
    #
    # with open('hull_list.txt', 'w') as f:
    #     for item in hull_list:
    #        f.write(item.__repr__() + "\n")

    f.close()

def get_arch_region_info_and_fill_result(obj, result, thickness=10.0,
                                         remove_nonconvex=False):
    """ compute arch region's properties like min, max pos along x, y
    :param obj:
    :param result:
    :return:
    """
    result.init_arch(Vector((0,0,0)))
    bm = bmesh_copy_from_object(obj)

    if len(bm.verts) < 3:
        return False

    ret = bmesh.ops.convex_hull(bm, input=bm.verts, use_existing_faces=False)
    bm_inner = ret['geom_interior']
    bm_hull = ret['geom']
    del ret

    # print(bm_inner)
    # print(bm_hull)

    # bm_hull_faces = [e for e in bm_hull if type(e) is bmesh.types.BMFace]
    bm_inner_vertices = [e for e in bm_inner if type(e) is bmesh.types.BMVert]

    # Create real mesh
    scene = bpy.context.scene
    temp_name = "~tmp~"
    me_new = bpy.data.meshes.new(name=temp_name)
    bm.to_mesh(me_new)

    obj_new = bpy.data.objects.new(name=me_new.name, object_data=me_new)
    scene.objects.link(obj_new)
    scene.update()
    ray_cast = obj_new.ray_cast
    EPS_BIAS = 0.000001

    convex_vertices = \
        set([e for e in bm_hull if type(e) is bmesh.types.BMVert])

    for v in bm_inner_vertices:
        no = v.normal
        no_sta = no * EPS_BIAS
        no_end = no * thickness

        p_a = v.co + no_sta
        p_b = v.co + no_end

        result_tmp, co2, no2, index = ray_cast(p_a, p_b)
        dist = (co2 - p_a).length

        # if we found thinner, it means convex vertices
        if index != -1:
            if dist < thickness:
                convex_vertices.add(v)
            # v.select_set(False)
        else:
            # v.select_set(True)
            pass

    print("convex_vertices...", len(convex_vertices))
    # print(convex_vertices)

    bmesh.ops.delete(bm, geom=list(convex_vertices), context=6)
    bmesh_to_object(obj_new, bm)
    bm.free()

    # Filtering clusters
    bpy.context.scene.objects.active = obj_new
    obj_new.select = True
    bpy.ops.mesh.separate(type='LOOSE')

    # deselect all
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')

    # filter out noisy objects
    arch_candidates = []
    objects = bpy.context.scene.objects
    filter_out_vtx = 100

    for ob in objects:
        # print(ob.name)
        if ob.type == 'MESH' and (temp_name in ob.name):
            if len(ob.data.vertices) < filter_out_vtx:
                ob.select = True
            else:
                arch_candidates.append(ob)

    if len(arch_candidates) < 1:  # in case of flat foot
        print("Error: No non-convex region, it may be near a flat foot")
        print("[Debug] Dumping out... ")
        print(arch_candidates)
        print(objects)

    else:
        # sort
        arch_candidates.sort(key=lambda o: len(o.data.vertices), reverse=True)
        if result is not None:
            result.arch_x_min_pos, result.arch_x_max_pos, \
            result.arch_y_min_pos, result.arch_y_max_pos, \
            result.arch_z_min_pos, result.arch_z_max_pos, \
            result.arch_center = \
                get_dim_pos(arch_candidates[0], with_center=True)

    if remove_nonconvex:
        for ob in arch_candidates:
            ob.select = True
        bpy.ops.object.delete()

        # remove the meshes, they have no users anymore.
        for item in bpy.data.meshes:
            if temp_name in item.name:
                bpy.data.meshes.remove(item)

    return len(arch_candidates) > 0


def run_operator_on_view3d(op, **kwargs):
    """
    :param op: operator
    :return:
    """

    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == 'VIEW_3D':
                ctx = bpy.context.copy()
                ctx['area'] = area
                print("area type= ", area.type)
                ctx['region'] = area.regions[-1]
                op(ctx, 'INVOKE_DEFAULT', **kwargs)
                area.tag_redraw()
                break


def get_view3d_context():
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == 'VIEW_3D':
                ctx = bpy.context.copy()
                return ctx

    return None


# Cleanup
def delete_modifiers(obj, name_to_del=_lattice_name):
    for mod in obj.modifiers:
        if mod.name == name_to_del:
            bpy.ops.object.modifier_remove(modifier=mod.name)


# Cleanup
def delete_lattice(mane_to_del=_lattice_name):
    bpy.ops.object.select_all(action='DESELECT')
    # change mode to object
    for ob in bpy.context.scene.objects:
        if mane_to_del in ob.name:
            ob.select = True
    bpy.ops.object.delete(use_global=False)


def create_lattice(obj, size, pos, u_size=6, v_size=10, w_size=2,
                   lattice_name=_lattice_name):
    # Create lattice and object
    lat = bpy.data.lattices.new(lattice_name)
    ob = bpy.data.objects.new(lattice_name, lat)

    loc = get_transformations(obj)[0]
    rot = get_transformations(obj)[1]
    scl = get_transformations(obj)[2]

    ob.location = pos
    # ob.location=(pos.x+loc.x,pos.y+loc.y,pos.z+loc.z)

    # size=values from selection bbox
    ob.scale = size
    # ob.scale=(size.x*scl.x, size.y*scl.y,size.z*scl.z)

    ob.rotation_euler = rot

    # Debug
    trans_mat = Matrix.Translation(loc)
    trans_mat *= Matrix.Scale(scl.x, 4, (1.0, 0.0, 0.0))
    trans_mat *= Matrix.Scale(scl.y, 4, (0.0, 1.0, 0.0))
    trans_mat *= Matrix.Scale(scl.z, 4, (0.0, 0.0, 1.0))

    print("trans mat", trans_mat)

    ob.show_x_ray = True
    # Link object to scene
    scn = bpy.context.scene
    scn.objects.link(ob)
    scn.objects.active = ob
    scn.update()

    # Set lattice attributes
    lat.interpolation_type_u = 'KEY_LINEAR'
    lat.interpolation_type_v = 'KEY_CARDINAL'
    lat.interpolation_type_w = 'KEY_BSPLINE'
    lat.use_outside = False
    lat.points_u = u_size
    lat.points_v = v_size
    lat.points_w = w_size

    # Set lattice points
    #    s = 0.0
    #    points = [
    #        (-s,-s,-s), (s,-s,-s), (-s,s,-s), (s,s,-s),
    #        (-s,-s,s), (s,-s,s), (-s,s,s), (s,s,s)
    #    ]
    #    for n,pt in enumerate(lat.points):
    #        for k in range(3):
    #            #pt.co[k] = points[n][k]
    return ob


def selected_verts_grp(obj):
    vertices = obj.data.vertices

    selverts = []

    if obj.mode == "EDIT":
        bpy.ops.object.editmode_toggle()

    for grp in obj.vertex_groups:

        if _verts_for_lattice_name in grp.name:
            bpy.ops.object.vertex_group_set_active(group=grp.name)
            bpy.ops.object.vertex_group_remove()

    tempgroup = obj.vertex_groups.new(_verts_for_lattice_name)

    # selverts=[vert for vert in vertices if vert.select==True]
    for vert in vertices:
        if vert.select == True:
            selverts.append(vert)
            tempgroup.add([vert.index], 1.0, "REPLACE")

            # print(selverts)

            #    print(type(selverts[0]))

    return selverts


def make_verts_grp(obj):
    """
    :param obj:
    :return: Vertex group name
    """

    vertices = obj.data.vertices

    if obj.mode == "EDIT":
        bpy.ops.object.editmode_toggle()

    for grp in obj.vertex_groups:

        if _verts_for_lattice_name in grp.name:
            bpy.ops.object.vertex_group_set_active(group=grp.name)
            bpy.ops.object.vertex_group_remove()

    temp_group = obj.vertex_groups.new(_verts_for_lattice_name)

    for vert in vertices:
        if vert.select == True:
            temp_group.add([vert.index], 1.0, "REPLACE")

    return _verts_for_lattice_name


def get_transformations(obj):
    rot = obj.rotation_euler
    loc = obj.location
    size = obj.scale

    return [loc, rot, size]


def find_bbox(obj, sel_verts_array, is_ave_center=False, debug=True):
    """
    :param obj:
    :param sel_verts_array:
    :param is_ave_center:
    :return: (min pos, max pos, size, middle)
    """
    mat = build_trans_scale_mat(obj)
    mat = obj.matrix_world

    minx = 0
    miny = 0
    minz = 0

    maxx = 0
    maxy = 0
    maxz = 0

    # Median Centers
    x_sum = 0
    y_sum = 0
    z_sum = 0

    c = 0
    for vert in sel_verts_array:
        # co=obj.matrix_world*vert.co.to_4d()

        co = vert.co
        # co=obj.matrix_world*vert.co

        x_sum += co.x
        y_sum += co.y
        z_sum += co.z

        if co.x < minx: minx = co.x
        if co.y < miny: miny = co.y
        if co.z < minz: minz = co.z

        if co.x > maxx: maxx = co.x
        if co.y > maxy: maxy = co.y
        if co.z > maxz: maxz = co.z

        # print("local cord", vert.co)
        # print("world cord", co)
        c += 1

    # DEBUG
    #     matrix_decomp=obj.matrix_world.decompose()
    # print ("martix decompose ", matrix_decomp)

    # Based on world coords
    minpoint = mat * Vector((minx, miny, minz))
    maxpoint = mat * Vector((maxx, maxy, maxz))

    if is_ave_center:
        middle = mat * Vector((x_sum / float(c),
                               y_sum / float(c), z_sum / float(c)))
    else:
        middle = (maxpoint + minpoint) / 2.

    size = maxpoint - minpoint
    size = Vector((abs(size.x), abs(size.y), abs(size.z)))

    #    minpoint=Vector((minx,miny,minz))
    #    maxpoint=Vector((maxx,maxy,maxz))
    #    middle=Vector( (x_sum/float(len(selvertsarray)), y_sum/float(len(selvertsarray)), z_sum/float(len(selvertsarray))) )
    #    size=maxpoint-minpoint
    #    size=Vector((abs(size.x),abs(size.y),abs(size.z)))
    # DEBUG
    #     bpy.context.scene.cursor_location=middle

    if debug:
        print("world matrix", obj.matrix_world)
        print("min - max", minpoint, " ", maxpoint)
        print("size", size)
        print("center point ->", middle)

    return minpoint, maxpoint, size, middle


def build_trans_scale_mat(obj):
    # This function builds a matrix that encodes translation and
    # scale and it leaves out the rotation matrix
    mat_trans = Matrix.Translation(obj.location)
    mat_scale = Matrix.Scale(obj.scale[0], 4, (1, 0, 0))
    mat_scale *= Matrix.Scale(obj.scale[1], 4, (0, 1, 0))
    mat_scale *= Matrix.Scale(obj.scale[2], 4, (0, 2, 0))

    mat_final = mat_trans * mat_scale

    print("mat_final", mat_final)
    return mat_final


def duplicate_and_rename(context, original_name, new_name):
    ob = context.scene.objects.get(original_name)

    # Create new mesh
    mesh = bpy.data.meshes.new(new_name)

    # Create new object associated with the mesh
    ob_new = bpy.data.objects.new(new_name, mesh)

    # Copy data block from the old object into the new object
    ob_new.data = ob.data.copy()
    ob_new.scale = ob.scale
    ob_new.location = ob.location

    # Link new object to the given scene and select it
    context.scene.objects.link(ob_new)
    ob_new.select = True
    context.scene.objects.active = ob_new
    context.scene.update()
    return ob_new


# based on BIBAL Sample insole model
def insole_breadth_on_length(length):
    m = 0.273
    c = 19.164
    return length * m + c


def get_std_insole_length(length, step=5):
    """
    :param length: Insole length
    :return:
    """
    length = int(length)
    if length % step == 0:
        return length
    else:
        return length + step - length % step


# Constants

def deform_insole(context, tag_LR, deform_limit=20, deform_weight_ratio=0.8):
    """
    Algorithms of generating insole...
        Adjust alignment.
        Check current conditions like existence of ref. insole
        Setup lattice size & pos. from current foot scan data
        Estimate how much deform with z-direction
         - Emphasize the position of arch center
        Deform or find proper ref. insole model
        Create modifier from th e duplicated ref model and lattice
        Deform the lattice

        ref_insole_name = _left_ref_insole_name
        insole_name = _left_insole_name
        foot_name = _left_foot_name
    """

    lattice_name = "lattice " + tag_LR
    foot_name = "foot " + tag_LR
    insole_name = "insole " + tag_LR
    ref_insole_name = "ref insole " + tag_LR

    obj_foot = context.scene.objects.get(foot_name)

    #  - Setup lattice size & pos. from current foot scan data
    delete_lattice(lattice_name)
    lat = create_lattice(obj_foot, obj_foot.dimensions, obj_foot.location,
                         u_size=7, v_size=11, w_size=1,
                         lattice_name=lattice_name)

    # - Estimate how much deform with z-direction
    la = lat.data
    # Control points
    cps = []
    for pt in la.points:
        cps.append(Vector((pt.co[0] * obj_foot.dimensions[0],
                           pt.co[1] * obj_foot.dimensions[1], 0.0)))

    def is_boundary(i):
        u = i % la.points_u
        v = i // la.points_u

        if u == 0:
            return True
        if u == (la.points_u - 1):
            return True

        if v == 0:
            return True
        if v == (la.points_v - 1):
            return True

        return False

    # compute z-direction distance
    context.scene.update()
    is_no_limit = True

    for i, v in enumerate(cps):
        # skip boundary
        if is_boundary(i):
            continue

        normal = Vector((0, 0, -1))
        v_start = v - 3.0 * normal  # back to 3 mm
        v_end = v + 50.0 * normal
        result_tmp, co2, no2, index = obj_foot.ray_cast(v_start, v_end)
        if index != -1:
            z_deform = (co2 - v_start).length
            if is_no_limit:
                v[2] = -z_deform
            else:
                if z_deform < deform_limit:
                    v[2] = -z_deform
        else:
            pass
        print(v)

    # copy ref insole to real insole
    obj_insole = duplicate_and_rename(context, ref_insole_name, insole_name)
    if obj_insole is None:
        print("[Error] Fail in finding insole object")
        return

    # Adjust insole size
    new_length = get_std_insole_length(obj_foot.dimensions[1])
    new_breadth = insole_breadth_on_length(new_length)

    print("[Debug] Adjusted dimension to change(l,b): ", new_length,
          new_breadth)

    new_dimensions = Vector((new_breadth, new_length, obj_insole.dimensions[2]))
    obj_insole.dimensions = new_dimensions
    context.scene.update()  # it should be called!!!!

    print("[Debug] Adjusted dimension: ", obj_insole.dimensions)

    # Reposition of lattice
    verts_selected = selected_verts_grp(obj_insole)
    size_lattice, pos_lattice = find_bbox(obj_insole, verts_selected)[2:]
    print("[Debug] Lattice size & pos.:", size_lattice, pos_lattice)

    # move insole instead of lattice
    # lat.location = pos_lattice
    obj_insole.location -= pos_lattice

    # lat.scale = size_lattice
    context.scene.update()

    modif = obj_insole.modifiers.new(lattice_name, "LATTICE")
    modif.object = lat
    modif.vertex_group = _verts_for_lattice_name

    # Deform Lattice
    for i, pt in enumerate(la.points):
        pt.co_deform[0] = cps[i][0] / obj_foot.dimensions[0]
        pt.co_deform[1] = cps[i][1] / obj_foot.dimensions[1]
        pt.co_deform[2] = cps[i][2] / obj_foot.dimensions[2] * \
                          deform_weight_ratio

    context.scene.update()
    # change mode to object
    bpy.ops.object.mode_set(mode='OBJECT')


def deform_insole_with_new_arch(context, tag_LR, arch_pos, new_height,
                                comp_ratio=1.0, deform_weight_ratio=0.8,
                                debug=True):
    '''
    arg:
    context -
    tag_LR - "left" or "right"
    arch_pos - new position of arch
    new_height - new arch height suggested by SRD
    comp_ratio - compensate ratio 0 ~ 1
    deform_weight_ratio - 0.8 deform ratio.
    '''

    if debug:
        print("Args...")
        print(arch_pos, new_height, comp_ratio)

    # retrieve existing objects
    insole_name = "insole " + tag_LR
    obj_insole = context.scene.objects.get(insole_name)
    if obj_insole is None:
        print("[Error] No object named as ", insole_name)
        return False

    lattice_name = "lattice " + tag_LR
    obj_lattice = context.scene.objects.get(lattice_name)
    if obj_lattice is None:
        print("[Error] No object named as ", lattice_name)
        return False

    foot_name = "foot " + tag_LR
    obj_foot = context.scene.objects.get(foot_name)
    if obj_foot is None:
        print("[Error] No object named as ", foot_name)
        return False

    arch_pos_lattice_2d = Vector((arch_pos[0] / obj_foot.dimensions[0],
                                  arch_pos[1] / obj_foot.dimensions[1]))

    ld = obj_lattice.data
    lattice_cell_width = 1.0 / ld.points_u
    lattice_cell_height = 1.0 / ld.points_v
    lattice_range = Vector((lattice_cell_width, lattice_cell_height)).length
    lattice_range = lattice_range * 1.5

    points_to_deform = {}
    for i, pt in enumerate(ld.points):
        # print("debug, {} co {}: co_deform {}".format(i, pt.co, pt.co_deform))
        d = Vector((pt.co.to_2d() - arch_pos_lattice_2d)).length
        if d < lattice_range:
            points_to_deform[i] = d

    if len(points_to_deform) < 1:
        print("[Error] No point to deform")
        return False

    import operator
    weight_sorted = sorted(points_to_deform.items(),
                           key=operator.itemgetter(1))

    # height diff. new - original
    d_height = (-new_height + arch_pos[2])*comp_ratio
    if debug:
        print("d_height =", d_height)

    d_height_lattice = d_height / obj_foot.dimensions[2] * deform_weight_ratio
    if debug:
        print("d_height_lattice =", d_height_lattice)

    for p in weight_sorted:
        original_z = ld.points[p[0]].co_deform[2]
        weight_pos = -1.0 / (lattice_range) * p[1] + 1.0
        new_z = d_height_lattice * weight_pos
        # deform. lattice
        # ld.points[p[0]].co_deform[2] = ((1.0 - comp_ratio) * original_z + \
        #                                 comp_ratio * new_z)
        ld.points[p[0]].co_deform[2] = new_z

    context.scene.update()
    # change mode to object
    bpy.ops.object.mode_set(mode='OBJECT')


def get_time_stamp():
    import time, datetime
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return st


# Test modules...
#

def test_lattice():
    # -----
    # Delete all the lattices for testing

    obj = bpy.context.active_object
    delete_modifiers(obj)
    sel_verts_array = selected_verts_grp(obj)
    size, pos = find_bbox(obj, sel_verts_array)[2:]

    print("Lattice size, pos", size, " ", pos)
    delete_lattice()
    lat = create_lattice(obj, size, pos,
                         u_size=7, v_size=11, w_size=1)

    modif = obj.modifiers.new(_lattice_name, "LATTICE")
    modif.object = lat
    modif.vertex_group = _verts_for_lattice_name

    bpy.context.scene.update()
    # bpy.ops.object.mode_set(mode='EDIT')


def test_fun():
    pt = Vector((-120.5989, -224.0855, -188.2310))
    center = [-125.64146219, -219.30198905, -190.81507575]
    normal = [-0.00979312, 0.48796617, 0.8728076]

    dist = dist_to_plane(np.array(pt), center, normal)

    print(dist)


def test_fun2():
    print("Test fun2 is run!")


def test_kdtree(context):
    """ KDtree Test"""
    obj = context.object

    # 3d cursor relative to the object data
    co_find = context.scene.cursor_location * obj.matrix_world.inverted()

    mesh = obj.data
    size = len(mesh.vertices)
    kd = kdtree.KDTree(size)

    for i, v in enumerate(mesh.vertices):
        kd.insert(v.co, i)

    kd.balance()

    # Find the closest point to the center
    co, index, dist = kd.find(co_find)
    print("Close to center:", co, index, dist)

    # Find the closest 10 points to the 3d cursor
    print("Close 10 points")
    for (co, index, dist) in kd.find_n(co_find, 10):
        print("    ", co, index, dist)

    # Find points within a radius of the 3d cursor
    print("Close points within 0.5 distance")
    co_find = context.scene.cursor_location
    for (co, index, dist) in kd.find_range(co_find, 0.005):
        print("    ", co, index, dist)


def test_raycast(context):
    pass


def test_error1():
    obj_insole = duplicate_and_rename(bpy.context, _left_ref_insole_name,
                                      _left_insole_name)


def test_error2():
    context = bpy.context
    # Adjust insole size
    obj_insole = get_left_insole(context)
    obj_foot = get_left_foot(context)
    new_length = get_std_insole_length(obj_foot.dimensions[1])
    new_breadth = insole_breadth_on_length(new_length)
    print("[Debug] Adjusted dimension to change(l,b): ", new_length,
          new_breadth)
    new_dimensions = Vector((new_breadth, new_length, obj_insole.dimensions[2]))
    obj_insole.dimensions = new_dimensions

    print("[Debug] Adjusted dimension: ", obj_insole.dimensions)


if __name__ == "__main__":
    # path = "C:\\Projects\\InsoleMaker\\scan_data\\foot1_150629\\0_Fused.obj"
    # load_obj(path)
    test_fun()


    pass
