__author__ = 'Jeff'

_url = "http://onestop.kriss.re.kr/srd_foot_api2.html?"
_arg = "req1=20&req2=1"

_codes = ["Regions", "Sample", "Ave.", "Std.", "Min", "Max",
          "1%", "5%", "25%", "50%", "75%", "95%", "99%"]
"""
    1번 : 부위를 나타내는 코드입니다.
    //1 발너비
    //2 발직선길이
    //3 아치높이
    //4 내측볼너비
    //5 외측볼너비
    //6 발등높이
    //7 발꿈치너비
    2번 : 측정인원수
    3번 : 평균
    4번 : 표준편차
    5번 : 최소값
    6번 : 최대값
    7번 : 1%
    8번 : 5%
    9번 : 25%
    10번 : 50%
    11번 : 75%
    12번 : 95%
    13번 : 99%
"""

_region_codes = {1: "Breadth", 2: "Length", 3: "Arch",
                 4: "내측볼너비", 5: "외측볼너비", 6: "발등높이", 7: "발꿈치너비"}


def print_list(li):
    for i in li:
        print(i)


def str_for_age_and_sex(age, sex):
    """
    :param age:
    :param sex: 1 is male, 2 is female
    :return: string requesting for age & sex
    """

    age = age // 10 * 10
    return "req1=" + str(age) + "&req2=" + str(sex)


def get_std_ref_keys():
    return _codes


def get_std_ref(age, sex, debug=False):
    '''
    :param age: age
    :param sex:
    :param debug:
    :return: breadth, length, arch dictionary
    '''
    from urllib.request import urlopen
    url_full = _url + str_for_age_and_sex(age, sex)

    breadth_db = {}
    length_db = {}
    arch_db = {}

    import ast

    try:
        with urlopen(url_full) as f:
            result = f.read().decode('euckr')

            for item in result.split("㏆㏆"):
                # print("---------------------------")
                db = None
                for i, s in enumerate(item.split("㎯")):
                    if i == 0:
                        if _region_codes[int(s)] == "Breadth":
                            db = breadth_db
                        elif _region_codes[int(s)] == "Length":
                            db = length_db
                        elif _region_codes[int(s)] == "Arch":
                            db = arch_db
                        else:
                            continue

                        db[_codes[i]] = _region_codes[int(s)]
                    else:
                        if db is not None:
                            db[_codes[i]] = ast.literal_eval(s)
                            # print(_codes[i], ": ", s)
    except ValueError:
        pass
    except Exception:
        from . import report
        report.log("KRISS SRD connection error!")

    if debug:
        print("Breadth DB --- ")
        print(breadth_db)
        print("---------------")
        print("Length DB  --- ")
        print(length_db)
        print("---------------")
        print("Arch DB    --- ")
        print(arch_db)
        print("---------------")

    return breadth_db, length_db, arch_db


# ----- 

def test_kriss(age=20, sex=1):
    from urllib.request import urlopen
    url_full = _url + str_for_age_and_sex(age, sex)

    with urlopen(url_full) as f:
        result = f.read().decode('euckr')
        for item in result.split("㏆㏆"):
            print("--------------------")
            for i, s in enumerate(item.split("㎯")):
                if i == 0:
                    try:
                        print(_codes[i], ": ", _region_codes[int(s)])
                    except ValueError:
                        break
                else:
                    print(_codes[i], ": ", s)


if __name__ == "__main__":
    from datetime import datetime

    print("Test started... ", datetime.now())

    # for i in range(0, 100, 10):
    #     print("**** Current Age: ", i)
    #     test_kriss(i)
    # get_std_ref(10, 1)
    test_kriss(20,1)